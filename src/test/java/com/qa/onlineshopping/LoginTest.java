package com.qa.onlineshopping;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class LoginTest {
	
	public WebDriver driver;
  
  @BeforeMethod
  public void setup() {
	  
	  
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to("http://demowebshop.tricentis.com/");
	  
	  
  }
  
  
  @Test
  public void login() {
	
	  driver.findElement(By.linkText("Log in")).click();
	  driver.findElement(By.id("Email")).sendKeys("uday.singh@testmail.com");
	  driver.findElement(By.id("Password")).sendKeys("Tosca12345!");
	  driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
	  
  }

  @AfterMethod
  public void tearDown() {
	  
	  driver.close();
	  
  }

}
